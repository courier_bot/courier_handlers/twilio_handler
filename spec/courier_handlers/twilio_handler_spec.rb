# frozen_string_literal: true

require 'courier_handlers/twilio_handler'

RSpec.describe CourierHandlers::TwilioHandler do
  let(:instance) { CourierHandlers::TwilioHandler.new }

  describe '.deliver' do
    subject { instance.deliver(fields) }

    before(:example) do
      CourierHandlers::TwilioHandler.account_sid = 'account_sid'
      CourierHandlers::TwilioHandler.auth_token = 'auth_token'
      CourierHandlers::TwilioHandler.default_from = '+15815551002'

      # Makes sure no calls to Twilio are actually made.
      allow(Twilio::REST::Client).to receive(:new).and_return(twilio_client)
      allow(twilio_client).to receive(:api).and_return(twilio_api)
      allow(twilio_api).to receive(:account).and_return(twilio_account_context)
      allow(twilio_account_context).to receive(:messages).and_return(twilio_message_list)
      allow(twilio_message_list).to receive(:create).and_return(twilio_response)
    end

    let(:twilio_client) { double(:twilio_client) }
    let(:twilio_api) { double(:twilio_api) }
    let(:twilio_account_context) { double(:twilio_account_context) }
    let(:twilio_message_list) { double(:twilio_message_list) }

    let(:fields) do
      {
        'from' => '+14185551000',
        'to' => '+14185551001',
        'content' => 'content'
      }
    end

    let(:twilio_response) do
      Twilio::REST::Api::V2010::AccountContext::MessageInstance.new(
        'v2010',
        'sid' => 'external_id'
      )
    end

    it 'creates the Twilio client with the correct credentials' do
      expect(Twilio::REST::Client).to(
        receive(:new)
          .with('account_sid', 'auth_token')
          .once
          .and_return(twilio_client)
      )

      subject
    end

    it 'requests a text message with the correct data' do
      expect(twilio_message_list).to(
        receive(:create)
          .with(from: '+14185551000',
                to: '+14185551001',
                body: 'content')
          .once
      )

      subject
    end

    it 'returns a normalized delivery response' do
      expect(subject).to be_a(CourierHandlers::DeliveryResponse)
      expect(subject.success).to eq(true)
      expect(subject.external_id).to eq('external_id')
    end

    context 'when the `from` field is not specified' do
      before(:example) { fields.delete('from') }

      it 'uses the configured `default_from` value' do
        expect(instance).to receive(:_deliver) do |fields|
          expect(fields['from']).to eq('+15815551002')
        end

        subject
      end
    end
  end

  describe '.callback' do
    subject { CourierHandlers::TwilioHandler.callback(args) }

    let(:args) do
      {
        'MessageSid' => 'external_id',
        'MessageStatus' => message_status
      }
    end

    let(:message_status) { 'delivered' }

    it 'returns a normalized callback request' do
      expect(subject).to be_a(CourierHandlers::CallbackRequest)
      expect(subject.success).to eq(true)
      expect(subject.external_id).to eq('external_id')
      expect(subject.error).to be_nil
      expect(subject.ignore?).to eq(false)
    end

    context 'when the message status is `undelivered`' do
      let(:message_status) { 'undelivered' }

      it 'sets the `success` attribute to `false`' do
        expect(subject.success).to eq(false)
      end
    end

    context 'when the message status is `failed`' do
      let(:message_status) { 'failed' }

      it 'sets the `success` attribute to `false`' do
        expect(subject.success).to eq(false)
      end
    end

    context 'when the message status is something else' do
      let(:message_status) { 'other' }

      it 'sets the callback request to ignore' do
        expect(subject).to be_a(CourierHandlers::CallbackIgnoreRequest)
        expect(subject.ignore?).to eq(true)
      end
    end

    context 'when an error message is included in the request' do
      before(:example) { args['ErrorMessage'] = 'error message' }

      it 'sets the `error` attribute' do
        expect(subject.error).to eq('error message')
      end
    end
  end
end
