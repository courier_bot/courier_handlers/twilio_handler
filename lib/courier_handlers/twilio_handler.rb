# frozen_string_literal: true

require 'courier_handlers/text_message_handler'
require 'twilio-ruby'

module CourierHandlers
  # Handler for the Twilio third-party service.
  class TwilioHandler < TextMessageHandler
    class << self
      attr_accessor :account_sid,
                    :auth_token,
                    :default_from

      def configure
        yield self
      end
    end

    # ref. https://www.twilio.com/docs/sms/api/message-resource#message-status-values
    # ref. https://www.twilio.com/docs/sms/api/message-resource#statuscallback-request-parameters
    # ref. https://www.twilio.com/docs/sms/send-messages#monitor-the-status-of-your-message
    #
    # An example of Twilio's callback request following a delivery:
    #     {
    #         "SmsSid": "SM...",
    #         "SmsStatus": "delivered",
    #         "MessageStatus": "delivered",
    #         "To": "+15558675310",
    #         "MessageSid": "SM...",
    #         "AccountSid": "AC...",
    #         "From": "+15017122661",
    #         "ApiVersion": "2010-04-01"
    #     }
    #
    # @params args [Hash] Twilio's callback body.
    # @return (see super)
    def self.callback(args)
      success = case args['MessageStatus']
                when 'delivered' then true
                when 'undelivered', 'failed' then false
                else return CourierHandlers::CallbackIgnoreRequest.new
                end

      CourierHandlers::CallbackRequest.new(
        success,
        args['MessageSid'],
        args['ErrorMessage']
      )
    end

    protected

    # The `from` field, if unset, will default to the configured one.
    #
    # Aside from the fields defined in the parent class, this handler does not normalize any other
    # fields.
    def normalize_delivery_fields(fields)
      fields['from'] ||= self.class.default_from
      super
    end

    # ref. https://www.twilio.com/docs/sms/send-messages#send-an-sms-with-twilios-api
    #
    # An example of Twilio's delivery response to a new message:
    #
    #     {
    #       "account_sid": "AC...",
    #       "api_version": "2010-04-01",
    #       "body": "McAvoy or Stewart? These timelines can get so confusing.",
    #       "date_created": "Thu, 30 Jul 2015 20:12:31 +0000",
    #       "date_sent": "Thu, 30 Jul 2015 20:12:33 +0000",
    #       "date_updated": "Thu, 30 Jul 2015 20:12:33 +0000",
    #       "direction": "outbound-api",
    #       "error_code": null,
    #       "error_message": null,
    #       "from": "+15017122661",
    #       "messaging_service_sid": "MG...",
    #       "num_media": "0",
    #       "num_segments": "1",
    #       "price": -0.00750,
    #       "price_unit": "USD",
    #       "sid": "MM...",
    #       "status": "sent",
    #       "subresource_uris": {
    #         "media": "/2010-04-01/Accounts/AC.../Messages/SM.../Media.json"
    #       },
    #       "to": "+15558675310",
    #       "uri": "/2010-04-01/Accounts/AC.../Messages/SM....json"
    #     }
    #
    # @params fields [Hash] the fields to give to the Twilio API in order to send a text message.
    #   Text message fields are all supported. The `from` field is optional - if not specified, the
    #   configured default phone number will be used.
    # @return (see super)
    def _deliver(fields)
      twilio_response = request_delivery(fields)

      CourierHandlers::DeliveryResponse.new(
        true,
        twilio_response.sid
      )
    end

    private

    def request_delivery(fields)
      Twilio::REST::Client
        .new(self.class.account_sid, self.class.auth_token)
        .api
        .account
        .messages
        .create(from: fields['from'],
                to: fields['to'],
                body: fields['content'])
    end
  end
end
