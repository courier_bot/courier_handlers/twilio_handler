# frozen_string_literal: true

module CourierHandlers
  module TwilioHandlers
    VERSION = '0.11.0'
  end
end
